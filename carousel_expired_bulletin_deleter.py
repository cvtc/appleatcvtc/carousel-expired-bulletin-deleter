import logging
from concurrent.futures import ThreadPoolExecutor
from datetime import datetime, timedelta, timezone
from os import environ

from carousel_api.carousel import Carousel

logging.basicConfig(
    level=logging.INFO, format="%(asctime)s - %(levelname)s - %(message)s"
)


def main():
    CAROUSEL_URL = environ["CAROUSEL_URL"]
    CAROUSEL_USERNAME = environ["CAROUSEL_USERNAME"]
    CAROUSEL_PASSWORD = environ["CAROUSEL_PASSWORD"]

    carousel = Carousel(CAROUSEL_URL, CAROUSEL_USERNAME, CAROUSEL_PASSWORD)

    # Create a datetime object with the date we want to delete bulletins from
    # before
    expired_threshold = datetime.now(tz=timezone.utc) - timedelta(days=30)

    # Get all zone IDs
    zone_ids = [zone["id"] for zone in carousel.get_zones()]

    # Get all bulletin json for each zone, using multithreading to increase
    # run time
    with ThreadPoolExecutor(max_workers=5) as executor:
        bulletins = []
        for result in executor.map(carousel.get_bulletins, zone_ids):
            bulletins.extend(result)

    # Sort through bulletin list and get any that are not alerts, are not
    # current (this will not match the Always Active bulletins), and were
    # set to expire over a month ago
    bulletins_to_delete = []
    for bulletin in bulletins:
        datetime_off = datetime.strptime(
            bulletin["DateTimeOff"], "%Y-%m-%dT%H:%M:%S"
        ).replace(tzinfo=timezone.utc)

        if not bulletin["IsAlert"] and bulletin["Status"] != "Current":
            if expired_threshold > datetime_off:
                bulletins_to_delete.append(bulletin["id"])

    # Break down the list of bulletins to delete into chunks of 100 to send
    # delete requests so the command doesn't time out
    chunk_size = 100
    id_chunks = [
        bulletins_to_delete[i: i + chunk_size]
        for i in range(0, len(bulletins_to_delete), chunk_size)
    ]

    # Delete bulletins
    logging.info(f"Queueing {len(bulletins_to_delete)} bulletins for deletion.")

    for id_chunk in id_chunks:
        carousel.delete_bulletins(id_chunk)

    logging.info("All bulletins queued for deletion.")


if __name__ == "__main__":
    main()
