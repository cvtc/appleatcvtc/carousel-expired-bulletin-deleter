import azure.functions as func

import carousel_expired_bulletin_deleter

app = func.FunctionApp()


@app.schedule(
    schedule="0 0 8 * * *", arg_name="myTimer", run_on_startup=False, use_monitor=False
)
def Daily(myTimer: func.TimerRequest) -> None:
    carousel_expired_bulletin_deleter.main()
