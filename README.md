Carousel Expired Bulletin Deleter
=======================================
Delete any bulletins in Carousel Signage that have been expired for over a month, are not current, and are not alerts.

## Reasoning for project
We had enough expired bulletins in our Carousel instance that it was causing load time issues for users. This will go through and automatically clean up any bulletins that have been lingering without needing to manually prune content.

## Requirements
Carousel service account with the following permissions:
* Zones
  * Read
* Bulletins
  * Read
  * Delete

## Usage

### GitLab
Uses base Azure Function setup, consult [general documentation.](https://gitlab.com/cvtc/appleatcvtc/project-templates/-/tree/main/Python%20Azure%20Functions?ref_type=heads)

## Variables

### CAROUSEL_URL
URL of your Carousel Signage instance
> https://example.carouselsignage.net

### CAROUSEL_USERNAME
Email that you use to sign into Carousel Signage account
> exampleemail@cvtc.edu

### CAROUSEL_PASSWORD
Password for the Carousel Signage account

## Authors
Bryan Weber (bweber26@cvtc.edu)

## License

[MIT © Chippewa Valley Technical College.](./LICENSE)
